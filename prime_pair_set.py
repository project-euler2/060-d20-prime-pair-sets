import math
from numba import jit
import time
start_time = time.time()

def is_prime(n):
	if n <= 1:
		return False
	if n == 2:
		return True
	if n > 2 and n % 2 == 0:
		return False
	max_div = math.floor(math.sqrt(n))
	for i in range(3, 1 + max_div, 2):
		if n % i == 0:
			return False
	return True

def Prime_Number_In_Range(a,n):
	prime = [True for i in range(n+1)]
	prime_values = []
	p = 2
	while(p * p <= n):
		if (prime[p] == True):# If prime[p] is not changed, then it is a prime
			for i in range(p * p, n + 1, p): # Update all multiples of p
				prime[i] = False
		p += 1
	for j in range(a,n):
		if prime[j]:
			prime_values.append(j)
	return prime_values

def find_5_set():
    somme = 50000
    all_primes = Prime_Number_In_Range(2,10000)
    all_primes.remove(2)
    all_primes.remove(5)
    for prime1 in all_primes:
        all_primes_minus1 = all_primes[:]
        all_primes_minus1.remove(prime1)
        for prime2 in all_primes_minus1:
            all_primes_minus2 = all_primes_minus1[:]
            all_primes_minus2.remove(prime2)
            if is_prime(int(str(prime1) + str(prime2))) and is_prime(int(str(prime2) + str(prime1))) and (prime1 + prime2) < somme:
                for prime3 in all_primes_minus2:
                    all_primes_minus3 = all_primes_minus2[:]
                    all_primes_minus3.remove(prime3)
                    if is_prime(int(str(prime3) + str(prime2))) and is_prime(int(str(prime2) + str(prime3)))\
                        and  is_prime(int(str(prime3) + str(prime1))) and is_prime(int(str(prime1) + str(prime3)))\
                            and prime1 + prime2 + prime3 < somme:
                        for prime4 in all_primes_minus3:
                            all_primes_minus4 = all_primes_minus3[:]
                            all_primes_minus4.remove(prime4)
                            if is_prime(int(str(prime3) + str(prime4))) and is_prime(int(str(prime4) + str(prime3)))\
                                and  is_prime(int(str(prime4) + str(prime1))) and is_prime(int(str(prime1) + str(prime4)))\
                                and  is_prime(int(str(prime4) + str(prime2))) and is_prime(int(str(prime2) + str(prime4)))\
                                and (prime1 + prime2 + prime3 + prime4) < somme:
                                for prime5 in all_primes_minus4:
                                    all_primes_minus5 = all_primes_minus4[:]
                                    all_primes_minus5.remove(prime5)
                                    if is_prime(int(str(prime3) + str(prime5))) and is_prime(int(str(prime5) + str(prime3)))\
                                        and  is_prime(int(str(prime5) + str(prime1))) and is_prime(int(str(prime1) + str(prime5)))\
                                        and  is_prime(int(str(prime5) + str(prime2))) and is_prime(int(str(prime2) + str(prime5)))\
                                        and  is_prime(int(str(prime5) + str(prime4))) and is_prime(int(str(prime4) + str(prime5)))\
                                        and (prime1 + prime2 + prime3 + prime4 + prime5) < somme:
                                            somme = (prime1 + prime2 + prime3 + prime4 + prime5)
                                            print(str(prime1) + ' : ' + str(prime2) + ' : ' + str(prime3) + ' : ' + str(prime4) + ' : ' + str(prime5) + ' somme = ' + str(somme))
                                            return somme

print(find_5_set())
print(f"--- {(time.time() - start_time):.10f} seconds ---" )