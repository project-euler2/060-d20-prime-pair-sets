---
The primes 3, 7, 109, and 673, are quite remarkable. By taking any two primes and concatenating them in any order the result will always be prime. For example, taking 7 and 109, both 7109 and 1097 are prime. The sum of these four primes, 792, represents the lowest sum for a set of four primes with this property.

Find the lowest sum for a set of five primes for which any two primes concatenate to produce another prime.
---

I'm not sure but I have an idea. In my mind, if the 4 primes given are the smallest to be give the right answer for 4 numbers, they must be part of the same set for 5 numbers. Which means that I just have to iterate through all the primes and find the lowest that can complete the set of these 4 numbers. Let's try that.

Okay so sadly it doesn't work because no prime below 1 million can add itself to the other. This means that I probably need to do something else and code it in a smart way

13 : 5197 : 5701 : 6733 : 8389 somme = 26033
26033